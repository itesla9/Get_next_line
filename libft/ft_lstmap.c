/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/02 12:19:54 by yteslenk          #+#    #+#             */
/*   Updated: 2016/12/03 17:14:50 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*tmp;
	t_list	*new;
	t_list	*prev;

	if (!lst)
		return (NULL);
	new = f(lst);
	prev = new;
	lst = lst->next;
	while (lst)
	{
		tmp = f(lst);
		prev->next = tmp;
		prev = prev->next;
		lst = lst->next;
	}
	return (new);
}
