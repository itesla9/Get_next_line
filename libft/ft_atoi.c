/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   atoi.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 13:01:37 by yteslenk          #+#    #+#             */
/*   Updated: 2016/12/02 17:00:15 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int	neg;
	int	res;

	neg = 0;
	res = 0;
	while (*str == ' ' || *str == '\t' || *str == '\r' || *str == '\f' ||
		*str == '\n' || *str == '\v')
		str++;
	if (*str == '-')
		neg++;
	if (*str == '-' || *str == '+')
		str++;
	while (*str >= '0' && *str <= '9')
	{
		res = res * 10 + (*str - '0');
		str++;
	}
	if (neg > 0)
		res = -res;
	return (res);
}
