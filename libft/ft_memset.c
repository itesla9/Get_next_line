/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memset.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 13:22:05 by yteslenk          #+#    #+#             */
/*   Updated: 2016/11/26 17:12:54 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memset(void *b, int c, size_t len)
{
	size_t num;

	num = len;
	while (len > 0)
	{
		*((unsigned char *)b) = (unsigned char)c;
		b++;
		len--;
	}
	while (num > 0)
	{
		b--;
		num--;
	}
	return (b);
}
