/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 19:15:39 by yteslenk          #+#    #+#             */
/*   Updated: 2016/12/02 17:14:30 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	size_t		ft_trimsize(char const *s)
{
	char const	*s2;
	size_t		len;
	size_t		res;

	s2 = s;
	len = 0;
	while (s[len])
		len++;
	while (*s == ' ' || *s == '\t' || *s == '\n')
		s++;
	while (*s2)
		s2++;
	while (*(s2 - 1) == ' ' || *(s2 - 1) == '\t' || *(s2 - 1) == '\n')
		s2--;
	res = s2 - s;
	if (len == res)
		return (1);
	else
		return (res);
}

static	int			ft_ifsymbol(char const *s)
{
	size_t	len;
	int		count;

	len = 0;
	count = 0;
	while (s[len])
	{
		if (s[len] != ' ' && s[len] != '\t' && s[len] != '\n')
			count++;
		len++;
		if (count > 0)
			return (1);
	}
	return (0);
}

char				*ft_strtrim(char const *s)
{
	size_t			len;
	unsigned int	i;

	len = 0;
	i = 0;
	if (!s)
		return (0);
	while (s[i] == ' ' || s[i] == '\t' || s[i] == '\n')
		i++;
	if (ft_ifsymbol(s) == 0)
		return (ft_strnew(len));
	if (ft_trimsize(s) == 1)
		return (ft_strdup(s));
	else
	{
		len = ft_trimsize(s);
		return (ft_strsub(s, i, len));
	}
}
