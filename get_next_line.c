/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/29 22:03:54 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/02 16:19:17 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int		ft_findn(char *location)
{
	int	i;

	i = 0;
	while (location[i])
	{
		if (location[i] == '\n')
			return (i);
		i++;
	}
	return (0);
}

char	*ft_big(char *location, char *tmp)
{
	int	n;

	if ((n = ft_findn(location)) != 0)
	{
		tmp = ft_strnew(n);
		n = 0;
		while (location[n] != '\n')
		{
			tmp[n] = location[n];
			n++;
		}
	}
	if ((n = ft_findn(location)) == 0)
		tmp = ft_strdup(location);
	return (tmp);
}

char	*ft_move(char *location)
{
	if ((ft_findn(location)) != 0)
	{
		while (*location != '\n')
			location++;
		location++;
	}
	else
		while (*location)
			location++;
	return (location);
}

char	*ft_string(char *location, int i, char *box)
{
	box[i] = 0;
	if (location == NULL)
	{
		location = ft_strnew(10000000);
		location = ft_strcat(location, box);
		return (location);
	}
	else if (location != NULL)
		location = ft_strcat(location, box);
	return (location);
}

int		get_next_line(int fd, char **line)
{
	static char	*location;
	int			i;
	char		box[BUFF_SIZE + 1];

	while ((i = read(fd, box, BUFF_SIZE)) && i != -1)
		location = ft_string(location, i, box);
	if (i == -1 || line == NULL || fd < 0)
		return (-1);
	if (i == 0 && location == NULL)
	{
		*line = ft_strnew(0);
		return (0);
	}
	if (*location == '\n')
	{
		*line = ft_strnew(0);
		location++;
		return (1);
	}
	*line = ft_big(location, *line);
	location = ft_move(location);
	if (**line != '\0')
		return (1);
	return (i);
}
